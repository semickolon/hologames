from screen import Screen, ScreenStack
from layer import Layer
from enum import Enum
from games.sploot import Sploot
from games.yeet import Yeet
from server import Server

import pyglet
from pyglet.window import Window
from pyglet.gl import *

import socket, threading

class DisplayMode(Enum):
    Normal = 0
    DebugMerged = 1
    DebugSplit = 2

class MainGame:
    def __init__(self):
        game = Sploot()

        pyglet.lib.load_library('avbin')
        pyglet.have_avbin = True
        Server.get()

        self.displayMode = DisplayMode.Normal
        pyglet.font.add_directory('assets/fonts')

        window_size = {
            DisplayMode.Normal: (1920, 1080),
            DisplayMode.DebugMerged: (1080, 960),
            DisplayMode.DebugSplit: (504, 896)
        }.get(self.displayMode)

        width, height = window_size
        self.window = Window(*window_size, fullscreen = self.displayMode == DisplayMode.Normal)
        self.screen_stack = ScreenStack(self, game)

        self.layers = {
            DisplayMode.Normal: [Layer(x = 900, y = 60, rotation = 90, flipX = True), 
                                 Layer(x = -60, y = 60, rotation = 90, flipX = True)],
            DisplayMode.DebugMerged: [Layer(), Layer()],
            DisplayMode.DebugSplit: [Layer(width, height / 2), 
                                     Layer(width, height / 2, y = height / 2)]
        }.get(self.displayMode)

    def update(self, dt):
        self.screen_stack.update(dt)

    def draw(self):
        self.window.clear()
        glEnable(pyglet.gl.GL_BLEND)
        glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)

        for i in reversed(range(len(self.layers))):
            layer = self.layers[i]
            glPushMatrix()
            layer.switch()
            self.screen_stack.draw(i)
            glPopMatrix()

        x_center = self.window.width / 2
        y_center = self.window.height / 2

        if self.displayMode == DisplayMode.Normal:
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2f', [x_center, 0, x_center, self.window.height]))
        elif self.displayMode == DisplayMode.DebugSplit:
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2f', [0, y_center, self.window.width, y_center]))