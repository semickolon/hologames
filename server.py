import socket, threading
from client import Client
from component import ObserverSubject

UDP_IP_ADDR = '192.168.254.106'
UDP_PORT = 52323

class Server:
    INSTANCE = None

    def __init__(self):
        self.clients = {} # Address as keys
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((UDP_IP_ADDR, UDP_PORT))

        self.conn_subject = ObserverSubject()
        self.key_event_subject = ObserverSubject()
        self.frame = 0

        socket_thread = threading.Thread(target = self.listen)
        socket_thread.daemon = True
        socket_thread.start()

    @staticmethod
    def get():
        if not Server.INSTANCE:
            Server.INSTANCE = Server()
        return Server.INSTANCE

    def client(self, player_index):
        for addr, client in self.clients.items():
            if client.player_index == player_index:
                return client
        # Dummy client
        return Client("0.0.0.0", player_index)

    def send_to(self, player_index, command, param):
        addr = self.client(player_index).addr
        self.send(addr, command, param)

    def send(self, addr, command, param):
        try:
            data = command + ":" + str(param)
            self.socket.sendto(str.encode(data), (addr, UDP_PORT))
            print("Sent to {}: {}".format(addr, data))
        except:
            print("Failed sending to {}".format(addr))

    def listen(self):
        print("Server listening at {}:{}".format(UDP_IP_ADDR, UDP_PORT))

        while True:
            data, (addr, port) = self.socket.recvfrom(1024)
            print("Received from {}: {}".format(addr, data.decode()))
            self.on_receive(addr, data.decode())

    def on_receive(self, addr, data):
        sep_index = data.index(':')
        command = data[:sep_index]
        param = data[(sep_index + 1):]

        if command == "CONN":
            if addr not in self.clients:
                self.new_client(addr)
            else:
                self.send(addr, "ERR", "Already connected")
            return

        if addr not in self.clients:
            self.send(addr, "ERR", "Not connected")
            return
        else:
            client = self.clients[addr]
        
        if command == "PRESS":
            key = param[0]
            down = param[1] == '1'
            client.set_key_state(key, down)
            self.key_event_subject.notify(client, (key, down))
        elif command == "DCONN":
            del self.clients[addr]
            self.conn_subject.notify(client, ClientConnectionEvent.DISCONNECTED)
        else:
            self.send(client.addr, "ERR", "Unknown command")

    def new_client(self, addr):
        player_index = len(self.clients)
        client = Client(addr, player_index)
        self.clients[addr] = client
        self.send(addr, "CONN", player_index)
        self.conn_subject.notify(client, ClientConnectionEvent.CONNECTED)

class ClientConnectionEvent:
    DISCONNECTED = 0
    CONNECTED = 1