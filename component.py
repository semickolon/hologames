import pyglet, math
from pyglet.sprite import Sprite
from pyglet.gl import *
from pyglet.text import Label
from random import randint

from layer import Layer

DEBUG_COLLIDERS = False

class Utils:
    @staticmethod
    def lerp(x0, x1, i):
        return x0 + ((x1 - x0) * i)

    @staticmethod
    def chance(c):
        return randint(1, 100) <= c

class GameObject(Sprite):
    def __init__(self, layer_index = 0, behaviors = [], active = True, visible = True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.layer_index = layer_index
        self.behaviors = []
        self.active = active
        self.visible = visible
        self.dead = False
        
        for behavior in behaviors:
            self.add_behavior(behavior)

    def add_behavior(self, behavior):
        self.behaviors.append(behavior)

    def remove_behavior(self, behavior):
        self.behaviors.remove(behavior)

    def get_behavior(self, T):
        for behavior in self.behaviors:
            if type(behavior) == T:
                return behavior
        return None

    def update_(self, dt):
        if self.active:
            behaviors = self.get_enabled_behaviors()
            for behavior in behaviors:
                behavior.pre_apply(self, dt)
            for behavior in behaviors:
                behavior.apply(self, dt)

    def must_draw(self):
        return self.active and self.visible

    def draw(self):
        if self.must_draw():
            super().draw()
            self.draw_behaviors()

    def draw_behaviors(self):
        for behavior in self.get_enabled_behaviors():
            behavior.draw(self)

    def get_enabled_behaviors(self):
        return [b for b in self.behaviors if b.enabled]

    def dispose(self):
        for behavior in self.behaviors:
            behavior.dispose()

class Behavior:
    def __init__(self, enabled = True):
        self.enabled = enabled
    def pre_apply(self, object, dt):
        pass
    def apply(self, object, dt):
        pass
    def draw(self, object):
        pass
    def dispose(self, object):
        pass

class State:
    def enter(self, object):
        pass
    def update(self, object, dt):
        pass

class StateMachine(Behavior):
    def __init__(self, initial_state, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.enter(initial_state)

    def enter(self, state):
        self.state = state
        self.has_new_state = True

    def apply(self, object, dt):
        if self.has_new_state:
            self.state.enter(object)
            self.has_new_state = False
        
        new_state = self.state.update(object, dt)
        if new_state is not None:
            self.enter(new_state)

class Collider(Behavior):
    def __init__(self, width = 0, height = 0, dx = 0, dy = 0, targets = [], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.width = width
        self.height = height
        self.dx = dx
        self.dy = dy
        self.bounds = Rectangle()
        self.targets = targets
        self.collisions = []

    def pre_apply(self, object, dt):
        self.collisions = []

    def apply(self, object, dt):
        self.bounds.x = object.x + self.dx
        self.bounds.y = object.y + self.dy
        self.bounds.width = self.width if self.width > 0 else object.width
        self.bounds.height = self.height if self.height > 0 else object.height
        targets = [t for t in self.targets if t not in self.collisions]
        
        for target in targets:
            if target in self.collisions:
                continue

            collider = target.get_behavior(Collider)

            if collider is not None and self.bounds.overlaps_with(collider.bounds):
                self.collisions.append(target)
                collider.collisions.append(object)

    def draw(self, player):
        if DEBUG_COLLIDERS:
            b = self.bounds

            if len(self.collisions) > 0:
                glColor4f(1, 0, 0, 0.5)
            else:
                glColor4f(1, 0, 1, 0.5)

            glRectf(b.x, b.y, b.x + b.width, b.y + b.height)
            glColor4f(1, 1, 1, 1)

class Rectangle:
    def __init__(self, x = 0, y = 0, width = 0, height = 0):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def overlaps_with(self, rect):
        if self.x > rect.x + rect.width or rect.x > self.x + self.width:
            return False
        if self.y + self.height < rect.y or rect.y + rect.height < self.y:
            return False
        return True

    def get_x0(self):
        return self.x + self.width

    def get_y0(self):
        return self.y + self.height

class ObserverSubject:
    def __init__(self):
        self.clear()

    def notify(self, object, event):
        for observer in self.observers:
            observer.on_notify(object, event)

    def add_observer(self, observer):
        self.observers.append(observer)

    def remove_observer(self, observer):
        self.observers.remove(observer)

    def clear(self):
        self.observers = []

class Observer:
    def on_notify(self, object, event):
        pass

class ParallaxSprite(GameObject):
    def __init__(self, scroll_width = Layer.DRAW_WIDTH, scroll_height = 0, x_vel = 0, y_vel = 0, running = True, *args, **kwargs):
        super(ParallaxSprite, self).__init__(*args, **kwargs)
        self.dx = 0
        self.dy = 0
        self.scroll_width = scroll_width
        self.scroll_height = scroll_height if scroll_height > 0 else self.image.height
        self.x_vel = x_vel
        self.y_vel = y_vel
        self.running = running

    def update_(self, dt):
        if self.running:
            self.dx += self.x_vel * dt
            self.dx %= self.width
            self.dy += self.y_vel * dt
            self.dy %= self.height

    def draw(self):
        orig_x = self.x
        orig_y = self.y
        y = 0 if self.y_vel > 0 else 1

        while self.get_y_coord(y) < self.scroll_height:
            y1 = self.get_y_coord(y) + orig_y
            x = 0

            while self.get_x_coord(x) < self.scroll_width:
                x1 = self.get_x_coord(x) + orig_x
                super().update(x = x1, y = y1)
                super().draw()
                x += 1
            
            y += 1

        super().update(x = orig_x, y = orig_y)

    def get_x_coord(self, n):
        return self.dx + (self.width * (n - 1))

    def get_y_coord(self, n):
        return self.dy + (self.height * (n - 1))

class TextView(GameObject):
    def __init__(self, label, *args, **kwargs):
        self.label = label
        dummy = pyglet.image.load('assets/null.png')
        super().__init__(img = dummy, *args, **kwargs)

    def update_(self, dt):
        super().update_(dt)

    def draw(self):
        if self.must_draw():
            self.label.draw()
        super().draw_behaviors()

    def set_text(self, text):
        self.label.text = text

class Interpolator:
    def of(self, x):
        return x

class BounceInterpolator(Interpolator):
    def __init__(self, bounces = 1, energy = 0.3):
        self.bounces = bounces
        self.energy = energy

    def of(self, x):
        return 1 + (-abs(math.cos(x * 10 * self.bounces / math.pi)) * self.curve(x))

    def curve(self, x):
        return -(2 * (1 - x) * x * self.energy + x * x) + 1

class SmoothInterpolator(Interpolator):
    def of(self, x):
        return math.cos((x + 1) * math.pi) / 2 + 0.5

class Animator(Behavior):
    def __init__(self, duration = 1.0, interpolator = Interpolator(), start = True, loop = False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.duration = duration
        self.interpolator = interpolator
        self.loop = loop
        self.playing = False

        if start:
            self.start()

    def start(self):
        self.t = 0
        self.playing = True

    def pause(self):
        self.playing = False

    def stop(self):
        self.t = 0
        self.playing = False

    def apply(self, object, dt):
        if not self.playing:
            return

        self.t += dt
        x = self.t / self.duration
        self.animate(object, self.interpolator.of(min(x, 1)))
        
        if x > 1:
            self.t = 0
            if not self.loop:
                self.playing = False

    def animate(self, object, value):
        pass

class FadeInAnimator(Animator):
    def __init__(self, object = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if object is not None:
            object.opacity = 0

    def animate(self, object, value):
        object.opacity = value * 255

class TranslateAnimator(Animator):
    def __init__(self, x, y, dx, dy, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.x0 = x
        self.y0 = y
        self.x1 = x + dx
        self.y1 = y + dy

    def animate(self, object, value):
        object.x = Utils.lerp(self.x0, self.x1, value)
        object.y = Utils.lerp(self.y0, self.y1, value)