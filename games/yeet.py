from pyglet.image import load
from screen import Screen
from component import *
from client import KeyCode
from server import Server
from layer import Layer

class Yeet(Screen):
    WINNING_SCORE = 5

    def __init__(self):
        p1 = Player(0)
        p2 = Player(1)
        top_border = Border(True)
        bottom_border = Border(False)
        targets = [p1, p2, top_border, bottom_border]
        ball = Ball(targets)
        txt_score_1 = TextView(Label(text = '', font_name = 'Small Pixel', font_size = 30, x = 1080 * 0.25, y = 100, anchor_x = 'center', anchor_y = 'center'), layer_index=1)
        txt_score_2 = TextView(Label(text = '', font_name = 'Small Pixel', font_size = 30, x = 1080 * 0.75, y = 100, anchor_x = 'center', anchor_y = 'center'), layer_index=1)

        self.logo = GameObject(img = load('assets/yeet/logo.png'), x = 399, y = 500)

        self.p1 = p1
        self.p2 = p2
        self.ball = ball
        self.txt_score_1 = txt_score_1
        self.txt_score_2 = txt_score_2
        self.playing = False
        self.ball.active = False

        self.ball.play_sound('bgm')

        super().__init__(
            GameObject(img = load('assets/yeet/bg.png'), layer_index = 1),
            p1, p2, top_border, bottom_border, ball,
            self.logo,
            txt_score_1, txt_score_2
        )

    def update(self, dt):
        super().update(dt)

        if self.playing:
            scorer_index = -1

            if self.ball.x <= 5:
                scorer_index = 1
                self.ball.reset()
            elif self.ball.x + self.ball.width >= Layer.DRAW_WIDTH - 5:
                scorer_index = 0
                self.ball.reset()

            if scorer_index >= 0:
                self.ball.play_sound('score')
                (self.p1 if scorer_index == 0 else self.p2).score += 1

            self.txt_score_1.set_text(str(self.p1.score))
            self.txt_score_2.set_text(str(self.p2.score))

            if self.p1.score == Yeet.WINNING_SCORE:
                self.game_over(0)
            elif self.p2.score == Yeet.WINNING_SCORE:
                self.game_over(1)
        else:
            if Server.get().client(0).is_key_down(KeyCode.X):
                self.start()

    def start(self):
        self.playing = True
        self.ball.active = True
        self.logo.visible = False

    def game_over(self, winner_index):
        self.playing = False
        self.ball.active = False
        loser_index = 1 if winner_index == 0 else 0

        (self.txt_score_1 if winner_index == 0 else self.txt_score_2).set_text('WIN')
        (self.txt_score_1 if winner_index == 1 else self.txt_score_2).set_text('LOSE')

        server = Server.get()
        server.send_to(winner_index, 'SOUND', 'WIN')
        server.send_to(loser_index, 'SOUND', 'LOSE')

        self.p1.score = 0
        self.p2.score = 0

        pass

class Player(GameObject):
    def __init__(self, index):
        self.index = index
        super().__init__(img = load('assets/yeet/p{}.png'.format(index + 1)))
        self.add_behavior(PlayerController())
        self.add_behavior(Collider(17, 121, 8, 8))
        self.y = 412
        self.score = 0
        if index == 1:
            self.x = Layer.DRAW_WIDTH - 33

class PlayerController(Behavior):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pad = 70
        self.min_y = pad
        self.max_y = Layer.DRAW_HEIGHT - pad - 137
        self.velocity = 500

    def apply(self, player, dt):
        client = Server.get().client(player.index)
        new_y = player.y
        
        if client.is_key_down(KeyCode.UP):
            new_y += self.velocity * dt
        elif client.is_key_down(KeyCode.DOWN):
            new_y -= self.velocity * dt

        new_y = min(self.max_y, new_y)
        new_y = max(self.min_y, new_y)
        player.y = new_y

class Ball(GameObject):
    def __init__(self, targets):
        super().__init__(img = load('assets/yeet/ball.png'))
        self.add_behavior(Collider(targets = targets))
        self.add_behavior(BallController())

    def reset(self):
        self.get_behavior(BallController).reset()

    def play_sound(self, name):
        pyglet.media.load('assets/yeet/sfx/{}.mp3'.format(name), streaming = False).play()

class BallController(Behavior):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial_velocity = 150
        self.velocity = self.initial_velocity
        self.acceleration = 1.02
        self.x_factor = 1
        self.y_factor = 1
        self.reset_pos = True

    def reset(self):
        self.velocity = self.initial_velocity
        self.x_factor = 1 if Utils.chance(50) else -1
        self.y_factor = 1 if Utils.chance(50) else -1
        self.reset_pos = True

    def apply(self, ball, dt):
        if self.reset_pos:
            self.reset_pos = False
            ball.x = (Layer.DRAW_WIDTH - 46) / 2
            ball.y = (Layer.DRAW_HEIGHT - 46) / 2
        else:
            collider = ball.get_behavior(Collider)

            if len(collider.collisions) > 0:
                collision = collider.collisions[0]

                if isinstance(collision, Border):
                    self.y_factor *= -1
                else:
                    self.x_factor *= -1

                ball.play_sound('hit')
                self.velocity *= self.acceleration

        ball.x += self.x_factor * self.velocity * dt
        ball.y += self.y_factor * self.velocity * dt

class Border(GameObject):
    def __init__(self, top):
        super().__init__(img = load('assets/yeet/border.png'), 
                         y = Layer.DRAW_HEIGHT - 50 if top else 0,
                         behaviors = [Collider()])