from screen import Screen
from client import KeyCode
from server import Server
from component import *

from pyglet.image import load, load_animation
from pyglet.image import Animation
from pyglet.gl import *
from pyglet.text import Label
from random import randint
import pyglet
        
class Sploot(Screen):
    MIN_OBSTACLE_DISTANCE = 300
    OBSTACLE_OFFSET_BOUNDS = (0, 500)

    def __init__(self):
        self.p1 = Player(0, 'bread')
        self.p2 = Player(1, 'lettuce')
        self.p2.x = 300
        self.players = [self.p1, self.p2]

        self.obstacles = []
        self.leaderboard = Leaderboard()
        self.playing = False
        self.score = 0

        title_img = load('assets/sploot/bg_title.png')
        title = GameObject(img = title_img, y = 600, x = (Layer.DRAW_WIDTH - title_img.width) / 2, layer_index = 1)
        title.add_behavior(FadeInAnimator(object = title))
        title.add_behavior(TranslateAnimator(title.x - 100, title.y, 100, 0, interpolator = SmoothInterpolator()))
        self.title = title

        mountains = ParallaxSprite(img = load('assets/sploot/bg_mountains.png'), x_vel = -50, running = False, y = 100, layer_index = 1)
        fence = ParallaxSprite(img = load('assets/sploot/bg_grass_fence.png'), x_vel = -550, running = False)
        self.fence = fence
        self.grounded_parallax_sprites = [mountains, fence]

        text_view = TextView(Label(text = 'Press X to Play', font_name = 'Small Pixel', font_size = 24, x = Layer.DRAW_WIDTH / 2, y = Layer.DRAW_HEIGHT / 2, 
                             anchor_x = 'center', anchor_y = 'bottom', align = 'center', width = 500, multiline = True))
        self.text_view = text_view

        super().__init__(
            GameObject(img = load('assets/sploot/bg_main.png'), layer_index = 1),
            ParallaxSprite(img = load('assets/sploot/bg_clouds.png'), x_vel = 25, y = 400),
            mountains,
            fence,
            title,
            text_view,
            self.p1,
            self.p2
        )

    def update(self, dt):
        super().update(dt)

        if self.playing:
            self.update_score(dt)
            self.update_obstacles()

            winner_index = -1

            if self.p1.is_dead():
                winner_index = 1
            elif self.p2.is_dead():
                winner_index = 0

            if winner_index >= 0:
                self.game_over(winner_index)
        else:
            client = Server.get().client(0)
            
            if client.is_key_pressed(KeyCode.X):
                self.start()

    def start(self):
        self.text_view.visible = 0
        self.title.visible = 0
        self.score = 0
        self.obstacles = []
        self.remove_objects(Obstacle)

        for player in self.players:
            Server.get().client(player.index).clear_presses()
            player.start()
        for sprite in self.grounded_parallax_sprites:
            sprite.running = True

        self.playing = True

    def game_over(self, winner_index):
        server = Server.get()
        server.send_to(winner_index, 'SOUND', 'WIN')

        for player in self.players:
            player.stop()
            i = player.index
            server.send_to(i, 'SOUND', 'WIN' if i == winner_index else 'LOSE')

        for sprite in self.grounded_parallax_sprites:
            sprite.running = False

        for obstacle in self.obstacles:
            if not isinstance(obstacle, FrisbeeObstacle):
                obstacle.active = False

        self.score = int(self.score)
        index = self.leaderboard.insert(self.score)

        message = "Player {} won".format(winner_index + 1)
        message += "\nScore: {}".format(self.score)
        message += "\nPress X to Play Again"
        message += "\n\nLeaderboard"

        for i, score in enumerate(self.leaderboard.scores):
            message += "\n{}. {}".format(i + 1, score)
            if i == index:
                message += " (NEW)"

        self.text_view.set_text(message)
        self.text_view.visible = True
        self.playing = False

    def update_score(self, dt):
        self.score += 10 * dt

    def update_obstacles(self):
        self.obstacles = [o for o in self.obstacles if not o.dead]
        do_generate = len(self.obstacles) == 0

        if len(self.obstacles) > 0:
            last_obstacle = self.obstacles[-1]
            bounds = last_obstacle.get_behavior(Collider).bounds
            distance_from_end = Layer.DRAW_WIDTH - bounds.get_x0()
            do_generate = distance_from_end >= Sploot.MIN_OBSTACLE_DISTANCE

        if do_generate:
            log = LogObstacle(self.fence, self.players)
            frisbee = FrisbeeObstacle(self.fence, self.players)
            obstacle = log if Utils.chance(70) else frisbee
            self.obstacles.append(obstacle)
            self.add_object(obstacle)

class Player(GameObject):
    SOUNDS = ['crash', 'jump', 'landing']

    def __init__(self, index, name, *args, **kwargs):
        self.index = index
        self.name = name
        self.stopped = True
        self.state_machine = StateMachine(IdleState())
        self.collider = Collider(100, 100, 20, 10)
        self.life_counter = LifeCounter(load('assets/sploot/life_{}.png'.format(name)))
        self.sounds = {}

        for sound_name in Player.SOUNDS:
            sound = pyglet.media.load('assets/sploot/sfx/{}_{}.mp3'.format(name, sound_name), streaming = False)
            self.sounds[sound_name] = sound
        
        super().__init__(img = load_animation('assets/sploot/{}_run.gif'.format(self.name)), 
                         x = 100, y = 90, 
                         behaviors = [self.state_machine, self.collider, self.life_counter])

    def is_dead(self):
        return self.get_behavior(LifeCounter).lives == 0

    def start(self):
        self.stopped = False
        self.life_counter.reset()

    def stop(self):
        self.stopped = True

    def play_sound(self, name):
        self.sounds[name].play()

    def on_crash(self):
        if not self.stopped:
            self.play_sound('crash')
            Server.get().send_to(self.index, 'VIBR', 400)

class Obstacle(GameObject):
    def __init__(self, img, parallax_sprite, players, width, height, dx = 0, dy = 0, *args, **kwargs):
        collider = Collider(width = width, height = height, dx = dx, dy = dy, targets = players)
        super().__init__(img = img, x = Layer.DRAW_WIDTH + randint(*Sploot.OBSTACLE_OFFSET_BOUNDS), y = 90, 
                         behaviors = [ParallaxTranslator(parallax_sprite), collider, KillIfNotInScreen()],
                         *args, **kwargs)
    
    def must_draw(self):
        return self.visible

class FrisbeeObstacle(Obstacle):
    def __init__(self, parallax_sprite, players, *args, **kwargs):
        super().__init__(img = load_animation('assets/sploot/frisbee.gif'), parallax_sprite = parallax_sprite,
                         width = 50, height = 13, players = players, *args, **kwargs)
        self.y = 160

class LogObstacle(Obstacle):
    def __init__(self, parallax_sprite, players, *args, **kwargs):
        super().__init__(img = load('assets/sploot/log.png'), parallax_sprite = parallax_sprite,
                         width = 83, height = 48, dx = 8, players = players, *args, **kwargs)

class KillIfNotInScreen(Behavior):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def apply(self, object, dt):
        bounds = object.get_behavior(Collider).bounds
        if bounds.get_x0() <= 0:
            object.dead = True

class ParallaxTranslator(Behavior):
    def __init__(self, parallax_sprite, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parallax_sprite = parallax_sprite
    
    def apply(self, object, dt):
        object.x += self.parallax_sprite.x_vel * dt

class Leaderboard:
    def __init__(self, size = 3):
        self.path = 'data/sploot_scores.txt'
        self.size = size

        file = open(self.path, 'r')
        data = file.readline()
        self.scores = []

        if len(data) > 0:
            self.scores = [int(n) for n in data.split(',')]

        file.close()

    def insert(self, score):
        index = 0

        for _score in self.scores:
            if score >= _score:
                break
            index += 1
        else:
            index = len(self.scores)

        if index < self.size:
            self.scores.insert(index, score)
            self.scores = self.scores[:self.size]
            data = ','.join([str(s) for s in self.scores])
            file = open(self.path, 'w')
            file.write(data)
            file.close()
            return index
        else:
            return -1

class LifeCounter(Behavior):
    def __init__(self, life_image, lives = 3, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.life_image = life_image
        self.max_lives = lives
        self.lives = lives
        self.invincible = False
        self.invincible_duration = 3
        self.invincible_flash = 0.25
        self.lives_y_offset = 150
        self.lives_ref_width = 150
        self.lives_pad = 8
        self.t = 0

    def reset(self):
        self.lives = self.max_lives
        self.invincible = False
        self.t = 0

    def apply(self, player, dt):
        opaque = True

        if self.invincible and not player.stopped:
            interval = self.t % (self.invincible_flash * 2)
            opaque = interval >= self.invincible_flash

        player.opacity = 255 if opaque else 128

        if self.invincible:
            self.t += dt
            if self.t >= self.invincible_duration:
                self.invincible = False

    def draw(self, player):
        if not self.invincible:
            collider = player.get_behavior(Collider)

            if (len(collider.collisions) > 0):
                player.on_crash()
                self.lives -= 1
                self.invincible = True
                self.t = 0

        if not player.stopped and self.lives > 0:
            lives_w = (self.lives * self.life_image.width) + (self.lives_pad * (self.lives - 1))
            lives_x = player.x + ((self.lives_ref_width - lives_w) / 2)
            lives_y = player.y + self.lives_y_offset
            
            for i in range(self.lives):
                x = lives_x + (i * self.life_image.width) + (i * self.lives_pad)
                self.life_image.blit(x, lives_y)

class IdleState(State):
    def enter(self, player):
        player.image = load_animation('assets/sploot/{}_idle.gif'.format(player.name))
        player.get_behavior(LifeCounter).do_draw = False
        player.get_behavior(LifeCounter).lives = 3

    def update(self, player, dt):
        if not player.stopped:
            return RunState()

class RunState(State):
    def enter(self, player):
        player.image = load_animation('assets/sploot/{}_run.gif'.format(player.name))
        player.get_behavior(Collider).height = 100
        player.get_behavior(LifeCounter).do_draw = True
        self.move_velocity = 200
        self.min_x = 50
        self.max_x = 500

    def update(self, player, dt):
        if player.stopped:
            return IdleState()

        client = Server.get().client(player.index)

        if client.is_key_down(KeyCode.LEFT):
            player.x -= self.move_velocity * dt
        elif client.is_key_down(KeyCode.RIGHT):
            player.x += self.move_velocity * dt

        player.x = max(self.min_x, player.x)
        player.x = min(self.max_x, player.x)

        if client.is_key_pressed(KeyCode.A):
            return JumpState()
        elif client.is_key_down(KeyCode.B):
            return DuckState()

class JumpState(State):
    def enter(self, player):
        player.image = load_animation('assets/sploot/{}_jump.gif'.format(player.name))
        player.get_behavior(Collider).height = 100
        player.play_sound('jump')
        self.t = 0
        self.by = player.y
        self.delay = 0

    def update(self, player, dt):
        client = Server.get().client(player.index)
        client.is_key_pressed(KeyCode.A) # Consume redundant jumps

        self.t += dt
        t = self.t - self.delay
        g = 2000
        v = 750
        dy = (v * t) - (g * t * t * 0.5)
        player.y = self.by + max(dy, 0)

        if dy <= 0 and self.t >= self.delay:
            return IdleState() if player.stopped else RunState()

class DuckState(State):
    def enter(self, player):
        seq_size = {
            'bread': 8,
            'lettuce': 7
        }
        images = [load('assets/sploot/{}_sploot/{}.png'.format(player.name, i)) for i in range(1, seq_size[player.name] + 1)]
        player.image = Animation.from_image_sequence(images, 0.1, False)
        player.get_behavior(Collider).height = 50
        player.play_sound('landing')
        self.t = 0
        self.delay = 1

    def update(self, player, dt):
        client = Server.get().client(player.index)
        self.t += dt

        if self.t >= self.delay:
            if not client.is_key_down(KeyCode.B):
                return IdleState() if player.stopped else RunState()
