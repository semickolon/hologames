class Client:
    def __init__(self, addr, player_index):
        self.addr = addr
        self.player_index = player_index
        self.key_states = {}
        self.prev_key_states = {}

    def set_key_state(self, key, down):
        self.prev_key_states[key] = self.is_key_down(key)
        self.key_states[key] = down

    def is_key_down(self, key):
        if key in self.key_states:
            return self.key_states[key]
        else:
            return False

    def is_key_previously_down(self, key):
        if key in self.prev_key_states:
            return self.prev_key_states[key]
        else:
            return False

    def is_key_pressed(self, key):
        if self.is_key_previously_down(key) and not self.is_key_down(key):
            self.prev_key_states[key] = False
            return True
        else:
            return False

    def clear_presses(self):
        self.prev_key_states = {}

class KeyCode:
    UP = 'U'
    DOWN = 'D'
    LEFT = 'L'
    RIGHT = 'R'
    HOME = 'H'
    X = 'X'
    Y = 'Y'
    A = 'A'
    B = 'B'
    