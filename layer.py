from pyglet.gl import *

class Layer:
    DRAW_WIDTH = 1080
    DRAW_HEIGHT = 960

    def __init__(
            self,
            width = DRAW_WIDTH, height = DRAW_HEIGHT, 
            x = 0, y = 0, 
            rotation = 0, 
            flipX = False, flipY = False
        ):
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.rotation = rotation
        self.flipX = flipX
        self.flipY = flipY

    def switch(self):
        x_center = Layer.DRAW_WIDTH / 2
        y_center = Layer.DRAW_HEIGHT / 2

        x_scale = self.width / Layer.DRAW_WIDTH
        y_scale = self.height / Layer.DRAW_HEIGHT

        glScalef(x_scale, y_scale, 1)
        glTranslatef(x_center + self.x * (1 / x_scale), y_center + self.y * (1 / y_scale), 0)
        glScalef(-1 if self.flipX else 1, -1 if self.flipY else 1, 1)
        glRotatef(self.rotation, 0, 0, 1)
        glTranslatef(-x_center, -y_center, 0)