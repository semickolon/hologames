class Screen:
    def __init__(self, *objects):
        self.objects = list(objects)

    def set_game(self, game):
        self.game = game

    def update(self, dt):
        self.objects = [o for o in self.objects if not o.dead]
        for obj in self.objects:
            obj.update_(dt)

    def add_object(self, object):
        self.objects.append(object)

    def remove_object(self, object):
        self.objects.remove(object)

    def get_objects(self, T):
        return [o for o in self.objects if isinstance(o, T)]

    def remove_objects(self, T):
        for object in self.get_objects(T):
            self.remove_object(object)

    def draw(self, layer_index):
        for obj in self.objects:
            if (obj.layer_index == layer_index):
                obj.draw()

    def dispose(self):
        for obj in self.objects:
            obj.dispose()

class ScreenStack:
    def __init__(self, game, screen):
        self.game = game
        self.screens = []
        self.push(screen)

    def push(self, screen):
        screen.set_game(self.game)
        self.screens.append(screen)

    def pop(self):
        if len(self.screens) > 1:
            self.screens[-1].dispose()
            del self.screens[-1]

    def current_screen(self):
        return self.screens[-1]

    def update(self, dt):
        self.current_screen().update(dt)
    
    def draw(self, layer_index):
        self.current_screen().draw(layer_index)