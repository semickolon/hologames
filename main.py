import pyglet
from game import MainGame

FRAMES_PER_SECOND = 60.0
game = MainGame()
window = game.window

@window.event
def on_draw():
    game.draw()

def update(dt):
    game.update(dt)

def main():
    pyglet.clock.schedule_interval(update, 1 / FRAMES_PER_SECOND)
    pyglet.app.run()

if __name__ == '__main__':
    main()